# val fs = FileSystem.get(sc.hadoopConfiguration)
# val dirSize = fs.getContentSummary(path).getLength
# val fileNum = dirSize/(512 * 1024 * 1024)  // let's say 512 MB per file

dirSize = subprocess.Popen(['hdfs', 'dfs', '-dus', source_path+'/*'], stdout=subprocess.PIPE)
fileNum = dirSize/(300*1024*1024)

df = sqlContext.read.parquet(path)
df.coalesce(fileNum).write.parquet(another_path)


# hadoop fs -cat /hdfs/output/part-r-* > /local/file.txt
