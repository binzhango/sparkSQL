from pyspark.sql import SparkSession
import time


time1 = time.time()
spark=SparkSession.builder.appName("pyspark") \
                        .enableHiveSupport() \
                        .getOrCreate()

src_cur = spark.sql("""
                        SELECT
                              
                        FROM
                               
                        WHERE
                               

""")




trg_pre = spark.sql("""
                    SELECT
                         
                    FROM
                           p
                    WHERE
                     client_code  ='UATGRACE'
                    AND as_at_tms='20180511154942'
""")

time3 = time.time()
print("load data time: {}".format(time3-time1))

incep_y = src_cur.filter(src_cur['inception_flag']=='Y')
incep_n = src_cur.filter(src_cur['inception_flag']=='N')

src = src_cur.alias('src')
trg = trg_pre.alias('trg')

df1 = trg.join(incep_y, incep_y['account_code']==trg['account_code'], 'leftanti')
df2 = df1.join(incep_n, (incep_n['account_code']==df1['account_code'])&(incep_n['effective_date']==df1['effective_date']), 'leftanti')

df = src.union(df2)

df.count()

time2 = time.time()

print("total time is {}".format(time2-time1))


# time1 = time.time()
#
#
# time1 = time.time()
#
# t1 = src.count()
# t2 = trg.count()
# print("Total src num {d}".format(t1))
# print("Total trg num {d}".format(t2))
# time2 = time.time()
# print("Time cost: {d}".format(time2-time1))
#
#
# num1 = trg.join(src, src['account_code']==trg['account_code'], 'leftanti').count()
# num2 = trg.join(src, src['account_code']==trg['account_code'], 'inner').count()
# print(num1)
#
# source1 = src_cur.filter(src_cur['inception_flag']=='Y').select('account_code')
# source2 = trg.join(source1, ((trg['account_code']==source1['account_code']) & (source1['account_code'].isNull())), 'left').select('trg.*')
#
# source3 = src_cur.filter(src_cur['inception_flag']=='N')
#
# a = source2.alias('a')
# b = source3.alias('b')
#
# df = a.join(b, (a['account_code']==b['account_code'])&(a['effective_date']==b['effective_date']), 'leftanti').select('a.*')
#
# result = df.union(src_cur)
# time2 = time.time()
# print(time2-tim1)
#
# result.collect()
#
# time3 =time.time()
# print(time3-time2)

spark.stop()
