
# hbase(main):008:0> scan 'test_hbase_table'
# ROW                                            COLUMN+CELL
#  dalin                                         column=cf:age, timestamp=1464101679601, value=40
#  tangtang                                      column=cf:age, timestamp=1464101649704, value=9
#  tangtang                                      column=cf:name, timestamp=1464108263191, value=zitang
# 2 row(s) in 0.0170 seconds



import json
host = '172.23.18.139'
table = 'test_hbase_table'
conf = {"hbase.zookeeper.quorum": host, "zookeeper.znode.parent": "/hbase-unsecure", "hbase.mapreduce.inputtable": table}
keyConv = "org.apache.spark.examples.pythonconverters.ImmutableBytesWritableToStringConverter"
valueConv = "org.apache.spark.examples.pythonconverters.HBaseResultToStringConverter"
hbase_rdd = sc.newAPIHadoopRDD(
        "org.apache.hadoop.hbase.mapreduce.TableInputFormat",
        "org.apache.hadoop.hbase.io.ImmutableBytesWritable",
        "org.apache.hadoop.hbase.client.Result",
        keyConverter=keyConv,
        valueConverter=valueConv,
        conf=conf)
hbase_rdd1 = hbase_rdd.flatMapValues(lambda v: v.split("\n"))


tt=sqlContext.jsonRDD(hbase_rdd1.values())

# In [113]: tt.show()
# +------------+---------+--------+-------------+----+------+
# |columnFamily|qualifier|     row|    timestamp|type| value|
# +------------+---------+--------+-------------+----+------+
# |          cf|      age|   dalin|1464101679601| Put|    40|
# |          cf|      age|tangtang|1464101649704| Put|     9|
# |          cf|     name|tangtang|1464108263191| Put|zitang|
# +------------+---------+--------+-------------+----+------+
