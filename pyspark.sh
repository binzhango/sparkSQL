#!/usr/bin/env bash
file=$1
echo ${file}
spark2-submit --master yarn-client –num-executors 2 --executor-memory 2g --executor-cores 2 ${file}
