# https://towardsdatascience.com/pyspark-udfs-and-star-expansion-b50f501dcb7b

import pyspark.sql.functions as f
import pyspark.sql.types as t
def my_function(arg1, arg2):
    argsum = arg1 + arg2
    argdiff = arg1 - arg2
    argprod = arg1 * arg2
    return argsum, argdiff, argprod
schema = t.StructType([
    t.StructField('sum', t.FloatType(), False),
    t.StructField('difference', t.FloatType(), False),
    t.StructField('product', t.FloatType(), False),
])
my_function_udf = f.udf(my_function, schema)
results_sdf = (
    sdf
    .select(
        my_function_udf(
            f.col('col1'), f.col('col2')
        ).alias('metrics'))   # call the UDF
    .select(f.col('metrics.*')) # expand into separate columns
)

# results_sdf.explain()
# BatchEvalPython [my_function(col1#87, col2#88), my_function(col1#87, col2#88), my_function(col1#87, col2#88)]
# That means that in order to do the star expansion on your metrics field, Spark will call your udf three times 
# — once for each item in your schema. This means you’ll be taking an already inefficient function and running it multiple times.

results_sdf = (
    sdf
    .select(
        f.explode(
            f.array(
                my_function_udf(f.col('col1'), f.col('col2'))
            )
        ).alias('metrics')
    )
    .select(f.col('metrics.*'))
)
