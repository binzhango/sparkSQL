from pyspark.context import SparkContext
sc = SparkContext('local', 'test')
b = sc.broadcast([1, 2, 3, 4, 5])
# b.value
# [1, 2, 3, 4, 5]
# sc.parallelize([0, 0]).flatMap(lambda x: b.value).collect()
# [1, 2, 3, 4, 5, 1, 2, 3, 4, 5]
# b.unpersist()
