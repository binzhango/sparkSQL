from pyspark.context import SparkContext
sc = SparkContext('local', 'test')

conf = {"kudu.mapreduce.master.address": "localhost",
            "kudu.mapreduce.input.table": "test",
           "kudu.mapreduce.column.projection": "key,value"
        }


rdd = sc.newAPIHadoopRDD("org.kududb.mapreduce.KuduTableInputFormat", "org.apache.hadoop.io.NullWritable", "org.kududb.client.RowResult", conf=conf)

print rdd.count()
